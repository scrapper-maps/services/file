const express = require('express')

global.env = require(`${__dirname}/config/env.json`)
global.__basedir = __dirname

const route = require(`${__dirname}/core/router`)()
const db = require(`${__dirname}/core/db`)
const models = require(`${__dirname}/core/models`)
const schemasRelation = require(`${__dirname}/app/schemas`);

const app = {
    start() {
        return new Promise(async (resolve, reject) => { resolve({ db : await db(), models: await models() }) }).then((data) => {
            const app = new express()
            const server = app.listen(env.app.port, env.app.host)
            app.use(`/${env.app.url}`, route)

            schemasRelation()
            return server
        });
    },
    processHandler : () => {
        process.on('exit', () => {
            console.log(`${env.app.id} with PID: ${process.pid} has been manually shutdown.`);
            app.exit();
        });
        process.on('uncaughtException', (err) => {
            console.log(`${env.app.id} with PID: ${process.pid} error: "${err}".`);
            app.exit();
        });
        process.on('warning', (warning) => {
            console.log(`${env.app.id} with PID: ${process.pid} warning : "${warning}".`);
        });
        process.on('SIGTERM', () => {
            console.log(`${env.app.id} PID: ${process.pid} terminated`);
            app.exit();
        });
        process.on('SIGHUP', () => {
            console.log(`${env.app.id} PID: ${process.pid} hanged up`);
            app.exit();
        });
        process.on('SIGINT', () => {
            console.log(`${env.app.id} PID: ${process.pid} interrupted`);
            app.exit();
        });
    },
    exit : () => {
        process.exit();
    }
}

app.start();
app.processHandler();