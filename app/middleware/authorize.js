`use strict`
const qs = require('querystring')
const { Validator: v } = require('node-input-validator')
const { validate } = require(`${__basedir}/app/helper/helper`)
const sequelize = require('sequelize')

const apiRoles = require(`./roles.json`)

const authorize = async (req, res, next) => {
    let authorizeApp = await db.application.findOne({ attributes: ['id', 'appname'], where: { secret: env.app.secret }, raw: true })
    await validate(authorizeApp, 'authentication')
    
    req.headers.authorization = req.query.token ? qs.stringify(req.query) : req.headers.authorization
    let validator = new v(req.headers, { authorization: 'required' })
    let matched = await validator.check()
    await validate(res, matched, 'Unauthorized', 403)

    const authorization = qs.parse(req.headers.authorization)
    let session = await db.authentication_user_login.findOne({ 
        where: { 
            appid: authorizeApp.id, 
            token: authorization.token, 
            create_at: { [sequelize.Op.gt]: new Date(Date.now() - (1 * 24 * 60 * 60 * 1000)) } 
        }
    })
    await validate(res, session, 'Unauthorized', 403)

    session.create_at = new Date()
    session = await session.reload()

    let roles = await db.authentication_user_role.findAll({ 
        attributes: ['rolename'], 
        where: { 
            appid: authorizeApp.id, 
            username: session.username, 
            statusid: 1 
        }, 
        raw: true 
    })
    roles = roles.map(({ rolename }) => rolename)
    let api = apiRoles[req.route.name]
    await validate(res, api, 'Unauthorized', 403)
    
    let validateRoles = api.filter((row) => {
        let valid = false
        for (let row of roles) {
            if(row == row) {
                valid = true
                break
            }
        }
        return valid
    })
    await validate(res, validateRoles.length, 'Unauthorized', 403)
    
    req.auth = { username: session.username, roles: roles, app: authorizeApp }
    next()
}

module.exports = authorize