'use strict';
module.exports = () => {
    /** User & Authentication */
    db.authentication_user.hasOne(db.s_personal_data, {as: 'spd', foreignKey : 'username', sourceKey : 'username'})
    db.s_personal_data.belongsTo(db.authentication_user, {as: 'au', foreignKey : 'username'})
    /** Keyword Instances */
    db.instances.belongsTo(db.keyword, { as: 'k', foreignKey : 'keyword_id'})
    db.keyword.hasMany(db.instances, { as: 'i', foreignKey : 'id'})
    /** Roles */
    db.authentication_role.hasMany(db.authentication_user_role, { as: 'aur', foreignKey : 'rolename'})
    db.authentication_user_role.belongsTo(db.authentication_role, { as: 'ar', foreignKey : 'rolename', targetKey: 'rolename'})
}
