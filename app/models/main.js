`use strict`
const { Validator: v } = require('node-input-validator')
const { validate } = require(`${__basedir}/app/helper/helper`)
const csvWriter = require('csv-writer').createObjectCsvWriter;
const fs = require('fs')

module.exports = {
    displayPicture: async (req, res) => {
        let account = await db.s_personal_data.findOne({
            attributes: [ 'photo' ],
            where: { appid: req.auth.app.id, username: req.auth.username, statusid: 1 },
            raw: true
        })
        res.sendFile(`${__basedir}/protected/account/${account.photo}`)
    },
    saveDisplayPicture: async (req, res) => {
        let validator = new v( { ...req.files, ...req.body }, {
            filename: 'required',
            picture: 'required',
            'picture.mimetype': 'in:image/jpg,image/jpeg,image/png'
        })
        await validate(res, await validator.check(), validator.errors)

        fs.rename(req.files.picture.file, `${__basedir}/protected/account/${req.body.filename}`, async (err) => {
            await validate(res, !err, 'Failed to save display picture')
            res.json({ status: 1 })
        })
    },
    adminDisplayPicture: async (req, res) => {
        let validator = new v(req.params, { id: 'required' })
        await validate(res, await validator.check(), validator.errors)

        let account = await db.s_personal_data.findOne({
            attributes: [ 'photo' ],
            where: { appid: req.auth.app.id, username: req.params.id, statusid: 1 },
            raw: true
        })

        res.sendFile(`${__basedir}/protected/account/${account.photo}`)
    },
    exportAccumulativeData: async (req, res) => {
        let validator = new v(req.query, { 
            order_by: 'in:instance_name,newest_first',
        })
        await validate(res, await validator.check(), validator.errors)

        const exportPath = `${__basedir}/protected/export/`
        const exportFileName = `export-orderby_${req.query.order_by}-search_${req.query.search}-${new Date}.csv`

        req.query.order_by = req.query.order_by == 'newest_first' ? 'create_at' : req.query.order_by

        let search = req.query.search ? {
            [Op.or]: [
                { instance_name: { [Op.iLike]: `%${req.query.search}%` } },
                { instance_address: { [Op.iLike]: `%${req.query.search}%` } },
                { instance_phone: { [Op.iLike]: `%${req.query.search}%` } },
            ]
        } : {}

        let instances = await db.instances.findAll({
            attributes: ['instance_name', 'instance_address', 'instance_phone', 'create_at'],
            where: { ...search, statusid: 1 },
            ... req.query.order_by && req.query.order_direction ? {
                order: [[req.query.order_by, req.query.order_direction]]
            } : {
                order: [['create_at', 'desc']]
            },
            raw: true
        })


        const exportCsv = csvWriter({
            path: exportPath + exportFileName,
            header: [
                {id: 'instance_name', title: 'Instance Name'},
                {id: 'instance_address', title: 'Instance Address'},
                {id: 'instance_phone', title: 'Instance Address'},
                {id: 'create_at', title: 'Instance Address'}
            ]
        })

        await exportCsv.writeRecords(instances)

        res.download(exportPath + exportFileName, exportFileName)
    }
}